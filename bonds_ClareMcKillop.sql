--1
select * from bond where CUSIP = '28717RH95'

--2
select * from bond order by maturity desc

--3
select sum(quantity * price) from bond

--4
select quantity * (coupon/100) from bond

--5
select * from bond where rating = 'AA2' OR rating = 'AA1' OR rating = 'AAA'

--6
select avg(price), avg(coupon), rating from bond group by rating 

--7
select id, CUSIP, (coupon/price) as Yield, b.rating, rating.expected_yield from bond b 
join rating on b.rating = rating.rating where (coupon/price) < expected_yield