--1
select * from trade t
join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
where p.trader_id = 1 and t.stock = 'MRK'

--2
select sum(t.size * t.price * (case when t.buy = 1 then -1 else 1 end)) as 'Profit or Loss' from trade t
join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
where p.trader_id = 1 and p.closing_trade_id is not null

--3
create view Traders_and_Profits as
select tr.first_name, tr.last_name, sum(t.size * t.price * (case when t.buy = 1 then -1 else 1 end)) as 'Profit or Loss' from trade t
join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
join trader tr on p.trader_id = tr.id
where p.closing_trade_id is not null group by tr.first_name, tr.last_name